$(document).ready(function(){
    $('#formSubmit').click(function(e){
        e.preventDefault();
        alert('Thank you for your feedback! We will get in touch with you in 24 hours.');
        $('#feedbackForm').trigger('reset');
    });
});